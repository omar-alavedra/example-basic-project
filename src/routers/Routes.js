import React from 'react';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom';
// import NotFoundPage from '../components/notFound';
import Home from '../pages/home';
import CategoryViewer from '../pages/categoryViewer';
import ProductViewer from '../pages/productViewer';
import Policy from '../pages/policy';
import NotFound from '../pages/notFound';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';

const Routes = (match) => {
    return(
        <Router>
            <div>
                {/* <ul>
                    <li>
                    <Link to="/">Home</Link>
                    </li>
                </ul> */}
                <Provider store={store}>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/collection/:category" component={CategoryViewer} />
                        <Route path="/collection/:category/:id" component={ProductViewer} />
                        <Route path="/politica-de-privacidad-tuprobador" component={Policy} />
                        <Route component={NotFound} />
                    </Switch>
                </Provider>
            </div>
        </Router>
    )
}

export default Routes;
