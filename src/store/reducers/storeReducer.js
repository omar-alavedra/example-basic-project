// import Item1 from '../../images/item1.jpg'
// import Item2 from '../../images/item2.jpg'
// import Item3 from '../../images/item3.jpg'
// import Item4 from '../../images/item4.jpg'
// import Item5 from '../../images/item5.jpg'
import Item1 from '../../components/images/third/camisetas/1.png';
import Item2 from '../../components/images/third/camisetas/2.png';
import Item3 from '../../components/images/third/camisetas/3.png';
import Item4 from '../../components/images/third/camisetas/4.png';
import Item5 from '../../components/images/third/camisetas/5.png';
import { SELECT_GENDER } from '../actions/actionTypes';


const initState = {
    gender: "FEMALE",
    category: 'camisetas',
    items: [
        {id:1,title:'PLAIN COTTON', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:110, size:"L", color: "red", img:Item1, images: [Item1,Item2,Item3,Item4] },
        {id:2,title:'COTTON SWEATER', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:80,size:"L", color: "red",img:Item2},
        {id:3,title:'PLAIN COTTON', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:110, size:"L", color: "red", img:Item3},
        {id:4,title:'COTTON LOGO', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:80,size:"L", color: "red",img:Item4},
        {id:5,title:'BLUE COTTON', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:110, size:"L", color: "red", img:Item5},
    ],

}
const productsReducer= (state = initState, action)=>{
    // console.log(action)
    switch (action.type) {
        case SELECT_GENDER:
            return {
                ...state,
                gender: action.gender
            }
        // case GET_TRIP:
        //     return {
        //         ...state,
        //         data: {
        //             ...state.data,
        //             trip: action.trip,
        //             tripLoaded: true,
        //             tripImages: action.images
        //         }
        //     }
        default:
            return state;
    }

}
export default productsReducer;