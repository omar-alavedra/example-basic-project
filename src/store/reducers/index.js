import { combineReducers } from 'redux';
import cartReducer from './cartReducer';
import storeReducer from './storeReducer';

const rootReducer = combineReducers({
    cartReducer: cartReducer,
    storeReducer: storeReducer,
});

export default rootReducer;