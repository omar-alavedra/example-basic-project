import { 
    ADD_ITEM_CART,
    SELECT_GENDER
} from  './actionTypes';

export const addToCart = (id) => {
    return {
        type: ADD_ITEM_CART,
        id: id
    }
}

export const selectGender = (gender) => {
    console.log(gender)
    return {
        type: SELECT_GENDER,
        gender: gender
    }
}