import React from 'react';
import { connect } from "react-redux";
import { isBrowser, isMobileOnly} from "react-device-detect";

// import { addToCart } from '../store/actions/index';

class ShoppingCart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayMenu: false,
        }
        this.showMenu = this.showMenu.bind(this);
    };
    showMenu = () => {
        this.setState({
            displayMenu: !this.state.displayMenu
        })
    }

    render() {
        let itemList = this.props.items.map(item=>{
            return(
                <div className="itemCard" key={item.id}>
                        <div>
                            {item.img}
                        </div>
                        <div className="card-image">
                            <span className="card-title">{item.title}</span>
                        </div>
                        <div className="card-content">
                            <p>{item.desc}</p>
                            <p><b>Price: {item.price}$</b></p>
                        </div>
                 </div>
            )
        })
        if(isBrowser){
            return(
                <div className="shoppingCart">
                    <div className="contentWrapper">
                    <h3 className="contentWrapper">Your Cart</h3>
                        {itemList}
                    </div>
                </div>
            )
        }
        if(isMobileOnly){

        }
        
    }
};


const mapStateToProps = (state)=>{
    console.log(state)
    return {
        items: state.cartReducer.items
    }
}

// const mapDispatchToProps= (dispatch)=>{
//     return{
//         addToCart: (id)=>{dispatch(addToCart(id))}
//     }
// }

export default connect(mapStateToProps/*,mapDispatchToProps*/)(ShoppingCart);