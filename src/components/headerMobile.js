import React from 'react';
// import { connect } from 'react-redux';
// import SignModal from '../components/signModal';

import { Link } from 'react-router-dom';
import HamburgerMenu from 'react-hamburger-menu';
// import HeaderMenuMobile from './headerMenuMobile';

class HeaderMobile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayMenu: false,
        }    
        this.showMenu = this.showMenu.bind(this);
    };
   
    showMenu = () => {
        this.setState({
            displayMenu: !this.state.displayMenu
        })
        // if(!this.state.displayMenu) {
        //     //menu is open
        // }
        // else {
        //     //menu is closed
        // }
    }
    render() {
        return(
            <div className="headerMobileBase">
                {/* {(this.state.loggedFunctionClicked&&(!this.props.logged))?<SignModal handleAnswer={this.handleModal}/>:null} */}
                <div className="menuMobile">
                    {/* { this.state.displayMenu ? <HeaderMenuMobile state={true}/> : <HeaderMenuMobile state={false}/> } */}
                </div>
                <header className={this.props.solid?"headerScrollMobile":"headerMobile"} style={{ boxShadow: 'no'}}>
                    <nav className="headerMobile__navigation">
                        <div className="logoMobile__wrapper">
                            <Link to="/">
                                <img className="logoMobile__top" src={require('./images/logo/tuprobadorNegro.png')}/>
                            </Link>
                        </div>
                        <div className="header__spacer"></div>
                        <div className="header__navlinks">
                                    <nav className="links__wrapper">
                                            <div className="link4">
                                                <HamburgerMenu
                                                    isOpen={this.state.displayMenu}
                                                    menuClicked={this.showMenu}
                                                    width={28}
                                                    height={25}
                                                    strokeWidth={2}
                                                    rotate={0}
                                                    color='white'
                                                    borderRadius={0}
                                                    animationDuration={0.5}
                                                />
                                            </div>
                                    </nav>
                                {/*<li> <NavLink to="/" activeClassName="is-active" exact={true}> Home </NavLink> </li>*/}
                        </div>

                    </nav>
                </header>
            </div>
        );
    }
};

export default HeaderMobile;