import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

//-- REDUX
import { selectGender } from '../store/actions/index';

//-- COMPONENTS
import ShoppingCart from '../components/shoppingCart';
// import HeaderMenu from './headerMenu';

//--3rd party components
import HamburgerMenu from 'react-hamburger-menu';


class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayMenu: false,
            displayCart: false
        }    
        this.showMenu = this.showMenu.bind(this);
        this.chooseGender = this.chooseGender.bind(this);
    };
    showMenu = () => {
        this.setState({
            displayMenu: !this.state.displayMenu
        })
    }

    showChart = () => {
        this.setState({
            displayCart: !this.state.displayCart
        })
    }
    chooseGender = (gender) => {
        console.log(gender)
        this.props.selectGender(gender)
    }
    render() {
        return(
            <div>
                {this.state.displayCart?<ShoppingCart/>:null}
                {/* {(this.state.loggedFunctionClicked&&(!this.props.logged))?<SignModal handleAnswer={this.handleModal}/>:null} */}
                <header className={"headerScroll"} style={{ boxShadow: 'no'}}>
                    <div className="menu">
                        {this.displayMenu}
                            {/* { this.state.displayMenu ? <HeaderMenu state={true}/> : <HeaderMenu state={false}/> } */}
                    </div>
                    <nav className="header__navigation">
                            <div className="header__navlinks">
                                        <nav className="links__wrapper">
                                            <div className="link1" onClick={()=>this.chooseGender("FEMALE")}>
                                                <Link to="/">MUJER</Link>
                                            </div>
                                            <div className="link__separator">
                                                |
                                            </div>
                                            <div className="link1" onClick={()=>this.chooseGender("MALE")}>
                                                <Link to="/">HOMBRE</Link>
                                            </div>
                                        </nav>
                                    {/*<li> <NavLink to="/" activeClassName="is-active" exact={true}> Home </NavLink> </li>*/}
                            </div>
                            <div className="header__spacer"></div>
                            <div className="logo__wrapper">
                                <Link to="/">
                                    <img className="logo__top" src={require('../components/images/logo/tuprobadorNegro.png')}/>
                                </Link>
                            </div>
                            <div className="header__navlinks">
                                        <nav className="links__wrapper">
                                            <div className="link1">
                                                <Link to="/" target="_blank">Sobre nosotros</Link>
                                            </div>
                                            <div className="link__separator">
                                                |
                                            </div>
                                            <div className="link3">
                                                {/* <Link to="https://app.sharryup.com/" target="_blank">Register</Link> */}
                                                <button onClick={()=>this.showChart()}>Carrito</button>
                                            </div>
                                                {/* <div className="link4">
                                                    <HamburgerMenu
                                                        isOpen={this.state.displayMenu}
                                                        menuClicked={this.showMenu}
                                                        width={28}
                                                        height={25}
                                                        strokeWidth={2}
                                                        rotate={0}
                                                        color='black'
                                                        borderRadius={0}
                                                        animationDuration={0.5}
                                                    />
                                                </div> */}
                                        </nav>
                                    {/*<li> <NavLink to="/" activeClassName="is-active" exact={true}> Home </NavLink> </li>*/}
                            </div>

                    </nav>
                </header>
            </div>
        );
    }
};

const mapStateToProps = (state)=>{
    // console.log(state)
    return {
        gender: state.storeReducer.gender
    }
}

const mapDispatchToProps= (dispatch)=>{
    return{
        selectGender: (gender)=>{dispatch(selectGender(gender))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);