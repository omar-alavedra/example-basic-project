import React from 'react'
import { connect } from "react-redux";
import { Link } from "react-router-dom";

//-- COMPONENTS -- desktop
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';

//--3rd party components
import { isBrowser, isMobileOnly} from "react-device-detect";

class CategoryViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };

        // this.newsLetterSubmit = this.newsLetterSubmit.bind(this);
    }
    displayItem = (id) => {
        // console.log(id);
        this.props.history.push({
            pathname: `/collection/${this.props.category}/${id}`,
            // state: { 
            //     trip: Trip,
            // }
        });
    }

    render() {
    if(isBrowser){
        let itemList = this.props.items.map(item=>{
            return(
                <div className="category" onClick={()=>this.displayItem(item.id)} style={{backgroundImage: `url(${item.img})`}} key={item.id}>
                    <div className="categoryTitle">
                        <p>{item.title}</p>
                    </div>                            
                </div>
            )
        })
        return (
            <div className="categoryViewer">
                <Header/>
                <div className="categoryHeader" >
                    <div className="categoriesWrapper" /*style={{backgroundImage: `url(${this.state.carouselImages[this.state.imageIndex].background})`}}*/ >
                        <div className="categories">
                            {itemList}
                        </div>
                    </div>
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                <Footer/>
            </div>
        )
    }
    if(isMobileOnly) {
        return (
            <div className="homeScreenMobile">
                    <div className="logoLandingMobile">
                        <img  src={require('../components/images/logo/tuprobadorBlanco.png')}/>
                    </div>
                    <div className="homeMobile__header" >
                        {/* <Player
                            playsInline
                            poster={require("../components/images/landing/captura.png")}
                            src="https://s3.eu-west-3.amazonaws.com/media.tuprobadorapp.com/3eb2c59a-1a24-4469-ad44-445cb57a6154.MP4"
                        /> */}
                    </div>   
                    <div className="appPromo__containerMobile">
                        <div className="home__containerMobile" >
                                <img className="infografiaMobile" src={require('../components/images/landing/infografia.png')} />
                                <div className="content__titleMobile">
                                    <p>Ahora, tienes dos opciones: <br/></p>
                                    <p id="bold">Seguir</p> <p>como hasta ahora,</p> <p id="bold"> perdiendo tiempo <br/> </p> <p> y dinero </p>
                                    <p id="bold">o </p> <p> sacar el </p><p id="bold">máximo partido</p> <p>a tu vida</p> <p id="bold"> disfrutando </p> <p>de tus compras. <br/></p>
                                                {/* <p>Solo unas pocas personas elegidas podrán ser los primeros en disfrutar del servicio totalmente gratis. Déjanos el email ahora. </p> */}
                                </div>
                        </div>
                        <div className="newsletterCallToActionMobile" style={{backgroundImage: `url(${this.state.wallpaper})`}}>
                        <div className="newsLetterTitleMobile">
                            <div className="title">
                                <h2>¡Únete a</h2>
                            </div>
                            <div className="title">
                                <h2>Tuprobador!</h2>
                            </div>
                        </div>
                        <div className="newsLetterSubtitleMobilee">
                            <br/>
                            <div className="subtitle">
                                <p>Solo unas personas elegidas podrán   </p>
                            </div>
                            <div className="subtitle">
                                <p>ser las primeras en disfrutar del  </p>
                            </div>
                            <div className="subtitle">
                                <p>servicio con un 50% de descuento.</p>
                            </div>
                            {/* <div className="subtitle">
                                <p id="bold">Déjanos el email ahora. </p>
                            </div> */}
                            
                        </div>
                        <div className="newsLetterInputMobile">
                            {/* <MailchimpSubscribe placeholder="info@sharryup.com" type="email" url={"this.state.url"} classname="gotIt"/> */}
                            {this.subscribeMailchimp()}
                            {/* <input onChange={this.hendleMailInput} placeholder="info@sharryup.com" type="email" id="newsLetterMail"></input><button onClick={this.newsLetterSubmit}>FORMAR PARTE</button> */}
                        </div>
                        {/* <div className={this.state.error.error?"newsLetterInputError":"newsLetterInputSent"}>
                            {this.state.error.display?this.state.error.value:null}
                        </div> */}
                        
                        <div className="downloadAppIconsMobile">
                            <div className="socialMediaIconsMobile">
                                <Link to="https://www.instagram.com/tu_probador/" target="_blank"> <img id="insta" src={require("../components/images/footer/instagram_icon.png")}/> </Link>
                                <Link to="https://www.facebook.com/TuProbador-403055473672917/" target="_blank"> <img id="fb" src={require("../components/images/footer/facebook-logo@3x.png")}/> </Link>
                                <Link to="https://www.youtube.com/watch?v=u965Y1pniM4" target="_blank"> <img id="ytb" className="youtube__icon" src={require("../components/images/footer/youtube-play-button@3x.png")}/> </Link>
                            </div>
                            {/* <div>bottom</div> */}
                        </div>

                        </div>
                        <FooterMobile/> 
                    </div>    
                    {/* <FooterMobile/> */}
                </div>
        )
    }
  }
}

const mapStateToProps = (state)=>{
    // console.log(state)
    return {
        items: state.storeReducer.items,
        category: state.storeReducer.category
    }
}

// const mapDispatchToProps= (dispatch)=>{
//     return{
//         addToCart: (id)=>{dispatch(addToCart(id))}
//     }
// }

export default connect(mapStateToProps/*,mapDispatchToProps*/)(CategoryViewer);