import React from 'react'
import { connect } from "react-redux";
import { Link } from "react-router-dom";

//-- COMPONENTS -- desktop
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';

//--3rd party components
import { isBrowser, isMobileOnly} from "react-device-detect";

class ProductViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            category: this.props.match.params.category
        };

        // this.newsLetterSubmit = this.newsLetterSubmit.bind(this);
    }

    render() {
    if(isBrowser){
        // console.log(this.props.items[this.state.id-1])
        // let itemList = this.props.items[this.state.id-1].images.map((item,count)=>{
        //     let url = require(item)
        //     return(
        //         <div className="item" style={{backgroundImage: item}} key={count}>
        //             <div className="categoryTitle">
        //                 <p>{item}</p>
        //             </div>                            
        //         </div>
        // )
        // })
        return (
            <div className="productViewer">
                <Header/>
                <div className="productWrapper" >
                    <div className="sidePanel">
                        <div className="backButton">
                            <Link to={`/collection/${this.props.category}/`}>ATRÁS</Link>
                        </div>
                        <div className="sidePanelContent">
                            {/* {itemList} */}
                        </div>
                    </div>
                    <div className="informationPanel">
                        information
                    </div>
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                <Footer/>
            </div>
        )
    }
    if(isMobileOnly) {
        return (
            <div className="homeScreenMobile">
            </div>
        )
    }
  }
}

const mapStateToProps = (state)=>{
    console.log(state)
    return {
        items: state.storeReducer.items,
        category: state.storeReducer.category
    }
}

// const mapDispatchToProps= (dispatch)=>{
//     return{
//         addToCart: (id)=>{dispatch(addToCart(id))}
//     }
// }

export default connect(mapStateToProps/*,mapDispatchToProps*/)(ProductViewer);