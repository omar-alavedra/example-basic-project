import React from 'react'
import '../styles/styles.scss';
import { Link } from "react-router-dom";
import { connect } from "react-redux";

//-- COMPONENTS -- desktop
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';
import CustomForm from '../components/subscribeMailForm';

//--3rd party components
import { Player } from 'video-react';
import "../../node_modules/video-react/dist/video-react.css";
import MailchimpSubscribe from "react-mailchimp-subscribe"

import { isBrowser, isMobileOnly} from "react-device-detect";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url : "//Tuprobador.us3.list-manage.com/subscribe/post?u=e50509d2eeb6f2cd25c6b9af2&id=07d2f88f87",        
            wallpaper : require('../components/images/landing/wallpaper4.jpg'),
            wallpaper1 : require('../components/images/third/categorias/trousers.png'),
            wallpaper2 : require('../components/images/third/categorias/shirts.png'),
            wallpaper3 : require('../components/images/third/categorias/calzado.png'),
            wallpaper4 : require('../components/images/third/categorias/accesorio.png'),
            categories: ["ropa" , "accesorios" , "calzado"],
        };

        // this.newsLetterSubmit = this.newsLetterSubmit.bind(this);
    }
    displayCategory = (id) => {
        console.log(id);
        this.props.history.push({
            pathname: `/collection/${id}/`,
            // state: { 
            //     trip: Trip,
            // }
        });
    }
    subscribeMailchimp = () => {
        return (
          <MailchimpSubscribe
            url={this.state.url}
            render={({ subscribe, status, message }) => (
              <CustomForm
                status={status}
                message={message}
                onValidated={formData => subscribe(formData)}
              />
            )}
          />
        );
    };

    render() {
    if(isBrowser){
        return (
            <div className="home">
                <Header/>
                <div className="homeHeader" >
                    <div className="categoriesWrapper" /*style={{backgroundImage: `url(${this.state.carouselImages[this.state.imageIndex].background})`}}*/ >
                        <div className="categories">
                            <div className="category" onClick={()=>this.displayCategory("pantalones")} style={{backgroundImage: `url(${this.state.wallpaper1})`}}>
                            <div className="categoryTitle">
                                    <p>PANTALONES</p>
                                </div>                            
                            </div>
                            <div className="category" onClick={()=>this.displayCategory("camisetas")} style={{backgroundImage: `url(${this.state.wallpaper2})`}}>
                                <div className="categoryTitle">
                                    <p>CAMISETAS</p>
                                </div>
                            </div>
                            <div className="category" onClick={()=>this.displayCategory("calzado")} style={{backgroundImage: `url(${this.state.wallpaper3})`}}>
                                <div className="categoryTitle">
                                    <p>CALZADO</p>
                                </div>                            
                            </div>
                            <div className="category" onClick={()=>this.displayCategory("accesorios")} style={{backgroundImage: `url(${this.state.wallpaper4})`}}>
                                <div className="categoryTitle">
                                    <p>ACCESORIOS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                <div className="appPromo__container">
                    <div className="home__container" >
                                    <img className="infografia" src={require('../components/images/landing/infografia.png')} />
                                    <div className="content__titlee">
                                        <p>Ahora, tienes dos opciones: <br/></p>
                                        <p id="bold">Seguir</p> <p>como hasta ahora,</p> <p id="bold"> perdiendo tiempo y dinero <br/></p>
                                        <p id="bold">o</p> <p> sacar el </p><p id="bold">máximo partido</p> <p>a tu vida</p> <p id="bold"> disfrutando </p> <p>de tus compras. <br/></p>
                                        {/* <p>Solo unas pocas personas elegidas podrán ser los primeros en disfrutar del servicio totalmente gratis. Déjanos el email ahora. </p> */}
                                    </div>
                    </div>
                </div>
                <div className="newsletterCallToAction" style={{backgroundImage: `url(${this.state.wallpaper})`}}>
                    <div className="newsLetterTitle">
                        <div className="title">
                            <h2>¡Únete a Tuprobador!</h2>
                        </div>
                        {/* <div className="title">
                            <h2> a Tuprobador </h2>
                        </div> */}
                        {/* <div className="title">
                            <h2> SHARRYUP </h2>
                        </div> */}
                    </div>
                    <div className="newsLetterSubtitle">
                        <br/>
                        <div className="subtitle">
                            <p>Solo unas personas elegidas podrán ser las primeras   </p>
                        </div>
                        <div className="subtitle">
                            <p> en disfrutar del servicio con un 50% de descuento.</p>
                        </div>
                        {/* <div className="subtitle">
                            <p id="bold">Déjanos el email ahora. </p>
                        </div> */}
                        
                    </div>
                    <div className="newsLetterInput">
                        {/* <MailchimpSubscribe placeholder="info@sharryup.com" type="email" url={"this.state.url"} classname="gotIt"/> */}
                        {this.subscribeMailchimp()}
                        {/* <input onChange={this.hendleMailInput} placeholder="info@sharryup.com" type="email" id="newsLetterMail"></input><button onClick={this.newsLetterSubmit}>FORMAR PARTE</button> */}
                    </div>
                    {/* <div className={this.state.error.error?"newsLetterInputError":"newsLetterInputSent"}>
                        {this.state.error.display?this.state.error.value:null}
                    </div> */}
                    <div className="newsLetterSubtitle">
                        
                        <div className="socialMediaIcons">
                            <Link to="https://www.instagram.com/tu_probador/" target="_blank"> <img id="insta" src={require("../components/images/footer/instagram_icon.png")}/> </Link>
                            <Link to="https://www.facebook.com/TuProbador-403055473672917/" target="_blank"> <img id="fb" src={require("../components/images/footer/facebook-logo@3x.png")}/> </Link>
                            <Link to="https://www.youtube.com/watch?v=u965Y1pniM4" target="_blank"> <img id="ytb" className="youtube__icon" src={require("../components/images/footer/youtube-play-button@3x.png")}/> </Link>
                        </div>
                    </div>
                    {/* <div className="downloadAppIcons">
                        <Link to="https://play.google.com/store/apps/details?id=com.sharryapp" target="_blank">
                            <img id="android" src={require('./images/landing/android-app-store-logo@3x.png')}/>
                        </Link>
                        <Link to="http://itunes.apple.com/app/id1438346353" target="_blank">
                            <img id="ios" src={require('./images/landing/apple-app-store-logo@3x.png')}/>
                        </Link>
                    </div> */}
                </div>
                <Footer/>
            </div>
        )
    }
    if(isMobileOnly) {
        return (
            <div className="homeScreenMobile">
                    <div className="logoLandingMobile">
                        <img  src={require('../components/images/logo/tuprobadorBlanco.png')}/>
                    </div>
                    <div className="homeMobile__header" >
                        {/* <Player
                            playsInline
                            poster={require("../components/images/landing/captura.png")}
                            src="https://s3.eu-west-3.amazonaws.com/media.tuprobadorapp.com/3eb2c59a-1a24-4469-ad44-445cb57a6154.MP4"
                        /> */}
                    </div>   
                    <div className="appPromo__containerMobile">
                        <div className="home__containerMobile" >
                                <img className="infografiaMobile" src={require('../components/images/landing/infografia.png')} />
                                <div className="content__titleMobile">
                                    <p>Ahora, tienes dos opciones: <br/></p>
                                    <p id="bold">Seguir</p> <p>como hasta ahora,</p> <p id="bold"> perdiendo tiempo <br/> </p> <p> y dinero </p>
                                    <p id="bold">o </p> <p> sacar el </p><p id="bold">máximo partido</p> <p>a tu vida</p> <p id="bold"> disfrutando </p> <p>de tus compras. <br/></p>
                                                {/* <p>Solo unas pocas personas elegidas podrán ser los primeros en disfrutar del servicio totalmente gratis. Déjanos el email ahora. </p> */}
                                </div>
                        </div>
                        <div className="newsletterCallToActionMobile" style={{backgroundImage: `url(${this.state.wallpaper})`}}>
                        <div className="newsLetterTitleMobile">
                            <div className="title">
                                <h2>¡Únete a</h2>
                            </div>
                            <div className="title">
                                <h2>Tuprobador!</h2>
                            </div>
                        </div>
                        <div className="newsLetterSubtitleMobilee">
                            <br/>
                            <div className="subtitle">
                                <p>Solo unas personas elegidas podrán   </p>
                            </div>
                            <div className="subtitle">
                                <p>ser las primeras en disfrutar del  </p>
                            </div>
                            <div className="subtitle">
                                <p>servicio con un 50% de descuento.</p>
                            </div>
                            {/* <div className="subtitle">
                                <p id="bold">Déjanos el email ahora. </p>
                            </div> */}
                            
                        </div>
                        <div className="newsLetterInputMobile">
                            {/* <MailchimpSubscribe placeholder="info@sharryup.com" type="email" url={"this.state.url"} classname="gotIt"/> */}
                            {this.subscribeMailchimp()}
                            {/* <input onChange={this.hendleMailInput} placeholder="info@sharryup.com" type="email" id="newsLetterMail"></input><button onClick={this.newsLetterSubmit}>FORMAR PARTE</button> */}
                        </div>
                        {/* <div className={this.state.error.error?"newsLetterInputError":"newsLetterInputSent"}>
                            {this.state.error.display?this.state.error.value:null}
                        </div> */}
                        
                        <div className="downloadAppIconsMobile">
                            <div className="socialMediaIconsMobile">
                                <Link to="https://www.instagram.com/tu_probador/" target="_blank"> <img id="insta" src={require("../components/images/footer/instagram_icon.png")}/> </Link>
                                <Link to="https://www.facebook.com/TuProbador-403055473672917/" target="_blank"> <img id="fb" src={require("../components/images/footer/facebook-logo@3x.png")}/> </Link>
                                <Link to="https://www.youtube.com/watch?v=u965Y1pniM4" target="_blank"> <img id="ytb" className="youtube__icon" src={require("../components/images/footer/youtube-play-button@3x.png")}/> </Link>
                            </div>
                            {/* <div>bottom</div> */}
                        </div>

                        </div>
                        <FooterMobile/> 
                    </div>    
                    {/* <FooterMobile/> */}
                </div>
        )
    }
  }
}

const mapStateToProps = (state)=>{
    // console.log(state)
    return {
        gender: state.storeReducer.gender
    }
}

// const mapDispatchToProps= (dispatch)=>{
//     return{
//         addToCart: (id)=>{dispatch(addToCart(id))}
//     }
// }

export default connect(mapStateToProps/*,mapDispatchToProps*/)(Home);